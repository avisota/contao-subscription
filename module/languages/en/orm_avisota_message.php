<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message']['setRecipients'] = array(
    'Select recipients',
    'Select the recipients for this newsletter.'
);
$GLOBALS['TL_LANG']['orm_avisota_message']['recipients']    = array(
    'Recipients',
    'Please chose the recipients for this newsletter.'
);
