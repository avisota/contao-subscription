<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-subscription
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Module
 */
$GLOBALS['TL_LANG']['MSC']['avisota-global-subscription']       = 'global subscription';
$GLOBALS['TL_LANG']['MSC']['avisota-global-subscription-label'] = 'global subscription';
