<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-subscription-recipient
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_subscription']['createdAt']     = array(
	'Created at'
);
$GLOBALS['TL_LANG']['orm_avisota_subscription']['updatedAt']     = array(
	'Last modified at'
);
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipientType'] = array(
	'Recipient type'
);
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipientId']   = array(
	'Recipient ID'
);
$GLOBALS['TL_LANG']['orm_avisota_subscription']['mailingList']   = array(
	'Subscribed mailing list',
);


/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_legend'] = 'Subscription';


/**
 * Reference
 */
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_list']['global']       = 'global subscription';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_list']['mailing_list'] = 'Mailing lists';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['orm_avisota_subscription']['show'] = array(
	'Subscription details',
	'Show the details of subscription ID %s'
);
