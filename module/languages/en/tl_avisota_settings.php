<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-core
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup']      = array(
    'Clean unconfirmed subscriptions',
    'Clean subscriptions that does not confirmed after certain days.'
);
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup_days'] = array(
    'Days',
    'Define after how many days unconfirmed subscriptions get removed.'
);

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_avisota_settings']['subscription_legend'] = 'Subscriptions';
