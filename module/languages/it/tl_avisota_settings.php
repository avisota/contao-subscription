<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:02:41+02:00
 */

$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup']['0']      = 'Pulisci sottoscrizioni non confermate';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup']['1']      = 'Pulisci le sottoscrizioni che non sono state confermate dopo un certo numero di giorni.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup_days']['0'] = 'Giorni';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup_days']['1'] = 'Definisci dopo quanti giorni le sottoscrizioni non confermate devono essere rimosse.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['subscription_legend']                    = 'Sottoscrizioni';
