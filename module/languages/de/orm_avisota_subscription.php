<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:16:30+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmationSent']['0']             = 'Bestätigung gesendet am';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmed']['0']                    = 'Bestätigt';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmed']['1']                    = 'Dieses Abonnement wurde bestätigt.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmedAt']['0']                  = 'Bestätigt am';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['createdAt']['0']                    = 'Erstellt am';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['delete']['0']                       = 'Abonnement löschen';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['delete']['1']                       = 'Abonnement ID %s löschen.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['edit']['0']                         = 'Abonnement bearbeiten';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['edit']['1']                         = 'Bearbeiten Sie das Abonnement ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['list']['0']                         = 'Abonnierte Liste';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['list']['1']                         = 'Bitte wählen Sie das Abonnement aus';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['mailingList']['0']                  = 'Abonnierte Mailingliste';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['new']['0']                          = 'Neues Abonnement';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['new']['1']                          = 'Fügen Sie ein neues Abonnement hinzu.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipient']['0']                    = 'Empfänger';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipientId']['0']                  = 'Empfänger-ID';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipientType']['0']                = 'Empfängertyp';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['reminderCount']['0']                = 'Erinnerung gesendet';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['reminderSent']['0']                 = 'Erinnerung gesendet am';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['show']['0']                         = 'Abonnementsdetails';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['show']['1']                         = 'Zeigen Sie die Details des Abonnements ID %s an.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['status_legend']                     = 'Status';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_legend']               = 'Abonnement';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_list']['global']       = 'Globales Abonement';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_list']['mailing_list'] = 'Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['token']['0']                        = 'Abonnements-Token';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['updatedAt']['0']                    = 'Zuletzt bearbeitet am';
