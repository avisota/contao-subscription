<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:16:40+01:00
 */

$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup']['0']      = 'Unbestätigte Abonnements bereinigen';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup']['1']      = 'Bereinigt Abonnements, die nicht nach der festgelegten Zahl von Tagen bestätigt wurden.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup_days']['0'] = 'Tage';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup_days']['1'] = 'Legen Sie fest, nach wievielten Tagen unbestätigte Abonnements bereinigt werden.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['subscription_legend']                    = 'Abonnements';
