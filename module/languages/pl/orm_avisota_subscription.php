<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-13T04:01:47+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmationSent']['0'] = 'Potwierdzenie wysłane';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmed']['0']        = 'Potwierdzone';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmed']['1']        = 'Ta subskrypcja została potwierdzona.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmedAt']['0']      = 'Potwierdzone';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['delete']['0']           = 'Usuń subskrypcję';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['delete']['1']           = 'Usuń subskrypcję ID %s';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['edit']['0']             = 'Edytuj subskrypcję';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['edit']['1']             = 'Edytuj subskrypcję ID %s';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['list']['0']             = 'Subskrybowana lista';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['list']['1']             = 'Proszę wybrać subskrybowaną listę';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['new']['0']              = 'Nowa subskrypcja';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['new']['1']              = 'Dodaj nową subskrypcję';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipient']['0']        = 'Odbiorca';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['reminderCount']['0']    = 'Przypomnienia wysłane';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['reminderSent']['0']     = 'Przypomnienie wysłane';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['status_legend']         = 'Status';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['token']['0']            = 'Token subskrypcji';
