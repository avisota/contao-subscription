<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:16:40+01:00
 */


$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup']['0']      = 'Far urden cun abunaments betg confermads';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup']['1']      = 'Stizzar abunaments sche na vegnan betg confermads enatifer in tschert temp.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup_days']['0'] = 'Dis';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_cleanup_days']['1'] = 'Definir suenter quant dis che abunents betg confermads vegnan allontanads.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['subscription_legend']                    = 'Abunaments';

