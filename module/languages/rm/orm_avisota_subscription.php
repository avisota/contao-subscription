<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-13T04:01:48+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmationSent']['0']             = 'Confermaziun tramess a';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmed']['0']                    = 'Confermà';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmed']['1']                    = 'L\'abunament è vegnì confermà.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['confirmedAt']['0']                  = 'Confermà las';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['createdAt']['0']                    = 'Creà las';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['delete']['0']                       = 'Stizzar l\'abunament';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['delete']['1']                       = 'Stizzar l\'abunament cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['edit']['0']                         = 'Modifitgar l\'abunament';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['edit']['1']                         = 'Modifitgar l\'abunament cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['list']['0']                         = 'Glista dad abunents';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['list']['1']                         = 'Tscherna la glista dals abunents.';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['mailingList']['0']                  = 'Glistas da mail abunadas';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['new']['0']                          = 'Nov abunament';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['new']['1']                          = 'Agiuntar in nov abunament';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipient']['0']                    = 'Destinatur';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipientId']['0']                  = 'ID dal destinatur';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['recipientType']['0']                = 'Tip da destinatur';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['reminderCount']['0']                = 'Tramess regurdanzas';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['reminderSent']['0']                 = 'Regurdanza tramess a ';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['show']['0']                         = 'Detagls da l\'abunament';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['show']['1']                         = 'Mussar ils detagls da l\'abunament cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['status_legend']                     = 'Status';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_legend']               = 'Abunament';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_list']['global']       = 'abunament global';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['subscription_list']['mailing_list'] = 'Glistas da mail';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['token']['0']                        = 'Token d\'abunar';
$GLOBALS['TL_LANG']['orm_avisota_subscription']['updatedAt']['0']                    = 'Ultima modificaziun las';

